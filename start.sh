#!/bin/bash

set -eu

mkdir -p /run /app/data/etcd/

export GO_VERSION=1.13.7
export GOROOT=/usr/local/go-${GO_VERSION}
export PATH=$GOROOT/bin:$PATH

echo "=> Generate configs"
cp /app/pkg/biz.toml /run/biz.toml
cp /app/pkg/sfu.toml /run/sfu.toml
cp /app/pkg/islb.toml /run/islb.toml

crudini --set /run/islb.toml redis addrs [\"${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}\"]
crudini --set /run/islb.toml redis password \"${CLOUDRON_REDIS_PASSWORD}\"
# crudini --set /run/sfu.toml webrtc.ice urls [\"stun:${CLOUDRON_STUN_SERVER}:${CLOUDRON_STUN_PORT}\",\"turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}\"]
# crudini --set /run/sfu.toml webrtc.ice urls [\"turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}\"]
# crudini --set /run/sfu.toml webrtc.ice username \"\"
# crudini --set /run/sfu.toml webrtc.ice credential \"${CLOUDRON_TURN_SECRET}\"

cat <<EOT >> /run/sfu.toml
[[webrtc.iceserver]]
urls = ["stun:${CLOUDRON_STUN_SERVER}:${CLOUDRON_STUN_PORT}"]

[[webrtc.iceserver]]
urls = ["turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}"]
credential = "${CLOUDRON_TURN_SECRET}"
EOT

echo "=> Ensure permissions"
chown -R cloudron:cloudron /run /app/data/

echo "=> Start supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i ion
