FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG GO_VERSION=1.13.7
ARG NATS_VERSION=2.0.0
ARG ETCD_VERSION=v3.4.7
ARG VERSION=add-turn-credentials

# Go (https://golang.org/dl/)
ENV GOROOT /usr/local/go-${GO_VERSION}
RUN mkdir -p /usr/local/go-${GO_VERSION} && \
    curl -L https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz | tar zxf - -C /usr/local/go-${GO_VERSION} --strip-components 1
ENV PATH $GOROOT/bin:$PATH

# RUN curl -L https://github.com/pion/ion/archive/v${VERSION}.tar.gz | tar zxf - --strip-components 1 -C /app/code/
RUN git clone https://github.com/pion/ion .
RUN make

# nats queue
RUN curl -L https://github.com/nats-io/nats-server/releases/download/v${NATS_VERSION}/nats-server-v${NATS_VERSION}-linux-amd64.zip -o /tmp/nats-server.zip && \
    cd /tmp && \
    unzip nats-server.zip && \
    cp nats-server-v${NATS_VERSION}-linux-amd64/nats-server /usr/bin && \
    rm -rf nats-server-v${NATS_VERSION}-linux-amd64 nats-server.zip


# etcd
RUN mkdir -p /tmp/etcd && \
    curl -L https://github.com/etcd-io/etcd/releases/download/${ETCD_VERSION}/etcd-${ETCD_VERSION}-linux-amd64.tar.gz | tar zxf - -C /tmp/etcd --strip-components 1 && \
    cp /tmp/etcd/etcd /usr/bin && \
    cp /tmp/etcd/etcdctl /usr/bin && \
    rm -rf /tmp/etcd

# patch webassets
RUN sed -i s/8443/443/g /app/code/web/ion-conference.js

# supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

COPY start.sh nginx.conf islb.toml sfu.toml biz.toml /app/pkg/

CMD [ "/app/pkg/start.sh" ]